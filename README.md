# MagicViewer
一款基于PyQt5的开源图片浏览器

可通过传递图片绝对路径调用，见test.py

原作者：https://github.com/createnewli/Magic-Viewer

2020/8/14 增加了幻灯片模式，修复程序因找不到对象的闪退问题
